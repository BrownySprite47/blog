$().ready(function () {
    $('form').validate({
        submitHandler: function (){
            $.ajax({
                type: 'POST',
                url: '/ajax/messages/add',
                data: $('form').serialize(),
                success: function(data) {
                    $('#progressbar').css('display', 'none');
                    if(data === 'success'){
                        $('.save_btn').css('display', 'none');
                        $('.finish_notice').html('Your message has been successfully added. <a href="/messages/add">Add more</a>').css('display', 'inline');
                    }
                    if(data === 'error'){
                        $('#mark_img').html('Неподдерживаемый формат!');
                        $('.finish_notice').html('An error has occurred. Please try again later.').css('display', 'inline');
                    }
                }
            });
        }
    });
});

function readImage (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.image_name').css('background-image', 'url(' + e.target.result + ')');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function upload($id_change, $progressbar, $id_preview, $counter){
    if($id_preview === undefined){ $id_preview = 'preview'; }
    if($counter === undefined){ $counter = ''; }

    $('#' + $progressbar).css('display', 'block');

    var progressBar = $('#' + $progressbar);
    var file_data = $('#' + $id_change).prop('files')[0];
    var form_data = new FormData();

    readImage(this);

    form_data.append('file', file_data);
    form_data.append('id', $counter);

    $.ajax({
        url: '/ajax/uploads/img',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        xhr: function(){
            var xhr = $.ajaxSettings.xhr();
            xhr.upload.addEventListener('progress', function(evt){
                if(evt.lengthComputable) {
                    var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
                    progressBar.val(percentComplete).text('Загружено ' + percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        success: function(data){
            if(data === 'format'){
                $('#' + $progressbar).css('display', 'none');
                $('#mark_img').html('Неподдерживаемый формат!');
            }else{
                if(data){
                    $('#' + $id_preview).html(data);
                    $('#mark_img').html('');
                    $('.notice_load').css('color', 'transparent');
                    $('.image_name').css('display', 'inline-block');
                    $('.load_file').css('display', 'none');
                    $('#' + $progressbar).css('display', 'none');
                }
            }
        },
    });
}


