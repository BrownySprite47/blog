$().ready(function () {
    $('form').validate({
        submitHandler: function (){
            $.ajax({
                type: 'POST',
                url: '/ajax/auth/login',
                data: $('form').serialize(),
                success: function(data) {
                    if(data === 'success'){
                        window.location.href = "/";
                    } else if(data === 'empty'){
                        $('#login_notice').html('Все поля обязательны для заполнения.');
                    } else if(data === 'error'){
                        $('#login_notice').html('Неверный логин или пароль');
                    }
                }
            });
        }
    });
});
