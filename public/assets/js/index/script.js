function send($numpage) {
    $.post(
        '/ajax/index',
        {
            sort: $('#sort').val(),
            numpage: $numpage,
        },
        AjaxSuccess
    );

    function AjaxSuccess(data) {
        $('#content-main').html(data);
    }
}