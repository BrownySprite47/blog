<div class="container">
    <h1 class="main_title">All messages</h1>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr class="success">
                <th>#</th>
                <th>Image</th>
                <th>User Name <span id="name_up" class="up_arrow sort_arrows"></span><span id="name_down" class="down_arrow sort_arrows"></span></th>
                <th>E-mail <span id="email_up" class="up_arrow sort_arrows"></span><span id="email_down" class="down_arrow sort_arrows"></span></th>
                <th>Homepage Url</th>
                <th>Message</th>
                <th>Ip Address</th>
                <th>Browser</th>
                <th>Date <span id="date_up" class="up_arrow sort_arrows"></span><span id="date_down" class="down_arrow sort_arrows"></span></th>

                <input type="hidden" id="sort">
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($data['messages'])): ?>
                <?php foreach($data['messages'] as $message): ?>
                <tr>
                    <td><?= $message['id'] ?></td>
                    <td><img class="message_img" src="<?= !empty($message['image']) ? CORE_IMG_PATH . '/' . $message['image'] : '/assets/images/img_not_found.png' ?>" alt=""></td>
                    <td><?= $message['user_name'] ?></td>
                    <td><?= $message['email'] ?></td>
                    <td><?= $message['homepage_url'] ?></td>
                    <td><?= $message['text'] ?></td>
                    <td><?= $message['ip_address'] ?></td>
                    <td><?= $message['browser'] ?></td>
                    <td><?= $message['pubdate'] ?></td>

                </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6">Нет сообщений</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?php if ($data['countpages'] > 1): ?>
            <div class="container-fluid">
                <div id="navigation" class="col-xs-12">
                    <nav class="nav_page" aria-label="Page navigation" style="text-align: center;">
                        <ul class="pagination">

                            <li <?= ($data['numpage'] <= 1 ? 'class="disabled"' : '') ?>>
                                <a href="javascript:void(0);" onclick="send(1);" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>

                            <?php
                            $limit = ($data['countpages'] < 5) ? $data['countpages'] : 5;
                            $left = $data['numpage'] - 2;
                            $right = $data['numpage'] + 2;

                            if ($left < 1) {
                                $left = 1;
                                $right = $left + $limit - 1;
                            }
                            if ($right > $data['countpages']) {
                                $right = $data['countpages'];
                                $left = $right - $limit + 1;
                            }
                            ?>

                            <?php for ($i = $left; $i <= $right; $i++): ?>
                                <li <?= ($data['numpage'] == $i ? 'class="active"' : '') ?>>
                                    <a href="javascript:void(0);" onclick="send(<?= $i ?>);"><?= $i ?></a>
                                    <input type="hidden" id="numpage" value="<?= $i ?>">
                                </li>
                            <?php endfor; ?>

                            <li <?= ($data['numpage'] == $data['countpages'] ? 'class="disabled"' : '') ?>>
                                <a href="javascript:void(0);" onclick="send(<?= $data['countpages'] ?>);" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<script>
    $(".sort_arrows").click(function() {
        $('#sort').val($(this).attr('id'))
        send($('#numpage').val());
    });
</script>