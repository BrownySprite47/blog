<?php include CORE_DIR . '/core/views/layouts/header/index.php'; ?>
    <div id="content-main">
        <div class="container-fluid" style="text-align: center; margin-top: 160px">
            <div class="col-lg-12">
                <p class="first_title">Добро пожаловать на «страницу 404»</p>
                <p class="second_title">К сожалению введенный вами адрес не доступен</p>
                <p class="reason">Скорее всего, это случилось по одной из следующих причин: <br>страница переехала, страницы больше нет, вы ошиблись
                    <br>в написании адреса или вам просто нравится изучать 404 страницы</p>
                <p class="links">Чтобы найти нужную страницу перейдите на главную</p>
                <a class="left_link" href="/">На главную</a>
            </div>
        </div>
    </div>
<?php include CORE_DIR . '/core/views/layouts/footer/index.php' ?>
