<?php include CORE_DIR . '/core/views/layouts/header/index.php'; ?>
<div id="content-main">
    <div class="container-fluid" style="text-align: center; margin-top: 160px">
        <div class="col-lg-12">
            <p class="first_title">Добро пожаловать на «страницу 500»</p>
            <p class="second_title">При обработке запроса произошла ошибка на сервере</p>
            <p class="reason">Попробуйте повторить ваши действия снова</p>
            <p class="links server">Или перейдите на одну из страниц</p>
            <a class="left_link" href="/">На главную</a>
        </div>
    </div>
</div>
<?php include CORE_DIR . '/core/views/layouts/footer/index.php' ?>
