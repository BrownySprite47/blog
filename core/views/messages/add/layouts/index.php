<div class="container">
    <h1 class="main_title">Add new message</h1>
    <form method="post" enctype="multipart/form-data" >
        <div class="col-lg-3">
            <div id="preview">
                <span class="image_name"></span>
                <input style="display: none" id="image_name" type="text"/>
            </div>
            <label for="user_picture" class="file_upload">
                <span class="load_file"></span>
                <input id="user_picture" type="file" name="image" onchange="upload('user_picture', 'progressbar', 'preview');"/>
            </label>
            <progress id="progressbar" value="0" max="100" style="display: none;"></progress>
        </div>
        <div class="col-lg-9">
            <div class="form-group">
                <label for="name">User Name</label>
                <input required type="text" id="name" value="<?= $_SESSION['login'] ?>" class="form-control" name="name" placeholder="Enter your name">
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input required type="email" id="email" class="form-control" name="email" placeholder="Enter your email">
            </div>
            <div class="form-group">
                <label for="url">Homepage Url</label>
                <input type="url" id="url" class="form-control" name="url" placeholder="Enter your homepage url">
            </div>
            <div class="form-group">
                <label for="message">Text</label>
                <textarea id="message" required class="form-control" name="text" rows="5" placeholder="Enter your message"></textarea>
            </div>
            <a href="javascript:void(0)"><input class="save_btn btn btn-success" type="submit" value="Save"></a>
            <p class="finish_notice"></p>
        </div>
    </form>
</div>