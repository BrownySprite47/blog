<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/assets/libs/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/css/index/style.css">
        <?php if (isset($data['css'])): ?>
            <?php foreach ($data['css'] as $css): ?>
                <link rel="stylesheet" href="/assets/<?=$css?>">
            <?php endforeach; ?>
        <?php endif; ?>
        <script src="/assets/libs/jquery/jquery-3.2.1.min.js"></script>
        <script src="/assets/libs/jquery/jquery.min.js"></script>
        <script src="/assets/libs/jquery/jquery.validate.js"></script>
        <title><?= isset($data['title']) ? $data['title'] : '' ?></title>
    </head>
    <body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Astrio</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Home <span class="sr-only">(current)</span></a></li>
                    <?php if (isset($_SESSION['id'])): ?>
                        <li><a href="/messages/add">Add Message</a></li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (isset($_SESSION['id'])): ?>
                        <li><a href="/auth/logout">Sign out</a></li>
                    <?php else: ?>
                        <li><a href="/auth/login">Sign in</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>

