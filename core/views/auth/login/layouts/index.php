<div class="container">
    <h1 class="main_title">Sign in</h1>
    <form method="post" enctype="multipart/form-data" >
        <div class="col-lg-4 col-lg-offset-4">
            <p id="login_notice"></p>
            <div class="form-group">
                <label for="login">Login</label>
                <input required type="text" id="login" class="form-control" name="login" placeholder="Enter your login">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input required type="password" id="password" class="form-control" name="password" placeholder="Enter your password">
            </div>
            <a class="login_btn" href="javascript:void(0)"><input class="btn btn-success" type="submit" value="Login"></a>
            <p class="finish_notice"></p>
        </div>
    </form>
</div>