<?php

/**
 * turn all entered by users into a string
 *
 * @param $value
 * @return string
 */
function clean($value)
{
    $link = dbConnect();

    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);
    $value = mysqli_real_escape_string($link, $value);

    return $value;
}



/**
 * Getting the data to create the sort.
 *
 * @param $data
 * @return mixed|string
 */
function getSqlSortString($data)
{
    $sort = [
        'name_up' => ' ORDER BY user_name ASC ',
        'name_down' => ' ORDER BY user_name DESC ',
        'email_up' => ' ORDER BY email ASC ',
        'email_down' => ' ORDER BY email DESC ',
        'date_up' => ' ORDER BY pubdate ASC ',
        'date_down' => ' ORDER BY pubdate DESC ',
    ];

    return (isset($sort[$data])) ? $sort[$data] : '';
}

/**
 * function to get the correct LIMIT with SQL query
 *
 * @param $start
 * @param $count
 * @return string
 */
function get_limit($start, $count)
{
    return $limit = ' LIMIT '.$start.', '.$count;
}

/**
 * Get the number of all records in the table.
 *
 * @param $table
 * @return mixed
 */
function db_count($table)
{
    $result = dbQuery('SELECT COUNT(1) FROM '.$table);

    $count = mysqli_fetch_array($result);

    return $count[0];
}

/**
 * Obtaining information about the number of pages.
 *
 * @param $data
 * @param $count
 * @param $table
 * @return mixed
 */
function getPaginationData($data, $count, $table)
{
    $data['countpages'] = intval((db_count($table) - 1) / $count) + 1;

    $data['numpage'] = intval((!isset($_POST['numpage']) ? 1 : $_POST['numpage']));

    if ($data['numpage'] < 1) {
        $data['numpage'] = 1;
    }
    if ($data['numpage'] > $data['countpages']) {
        $data['numpage'] = $data['countpages'];
    }

    $data['startproject'] = $data['numpage'] * $count - $count;

    return $data;
}

/**
 * Getting information about the file extension
 *
 * @param $str
 * @return bool|string
 */
function getExtension($str)
{
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i+1, $l);
    return $ext;
}