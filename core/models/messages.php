<?php

/**
 * Obtaining information about all messages with a specified limit.
 *
 * @param $limit
 * @return array
 */
function getAllMessages($limit)
{
    $messages = getData(dbQuery("SELECT * FROM messages ".$limit));

    array_pop($messages);

    return $messages;
}

/**
 * Obtaining information about all messages with a specified limit and sorting.
 *
 * @param $data
 * @param $limit
 * @return array
 */
function getAllSortMessages($data, $limit)
{
    if (isset($data['sort'])) {
        $sort = getSqlSortString($data['sort']);
        $messages = getData(dbQuery("SELECT * FROM messages ".$sort.$limit));

        array_pop($messages);
    } else {
        $messages = [];
    }

    return $messages;
}

/**
 * Adding a message to the database
 *
 * @param $data
 * @return bool|int|mysqli_result|string
 */
function addMessage($data)
{
    return dbQuery("INSERT INTO `messages`(`author_id`,`user_name`, `email`, `homepage_url`, `text`, `image`, `ip_address`, `browser`) 
           VALUES ('" . $_SESSION['id'] . "', '" . clean($data['name']) . "', '" . clean($data['email']) . "', '" . clean($data['url']) . "',
            '" . clean($data['text']) . "', '" . clean($data['image']) . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $_SERVER['HTTP_USER_AGENT'] . "')");
}
