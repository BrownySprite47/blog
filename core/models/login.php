<?php

/**
 * Authorization of the user in the system
 *
 * @param $data
 * @return bool
 */
function userLogin($data)
{
    $user = getUserData($data);

    if (isset($user[0]['id'])) {
        $_SESSION['id'] = $user[0]['id'];
        $_SESSION['login'] = $user[0]['login'];
        return true;
    }
    return false;
}

/**
 * Getting information about the user.
 *
 * @param $data
 * @return array
 */
function getUserData($data)
{
    return getData(dbQuery("SELECT * FROM users WHERE login = '" . clean($data['login']) . "' AND password = '" . clean($data['password']) . "' LIMIT 1"));
}

