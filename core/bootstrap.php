<?php
define("CORE_DIR", __DIR__ . '/../');


require_once CORE_DIR . '/core/configs/main.php';

session_start();

require_once CORE_DIR . '/core/library/validation.php';

require_once CORE_DIR . '/core/models/main.php';
require_once CORE_DIR . '/core/models/login.php';
require_once CORE_DIR . '/core/models/messages.php';

require_once CORE_DIR . '/core/library/database.php';
require_once CORE_DIR . '/core/library/main.php';
require_once CORE_DIR . '/core/library/router.php';



