<?php

/**
 * Page /auth/login
 */
function index()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (!isset($_POST['login']) || !isset($_POST['password']) || empty($_POST['login']) || empty($_POST['password'])) {
            exit('empty');
        } else {

            $data = [
                'login' => clean($_POST['login']),
                'password' => clean($_POST['password']),
            ];

            $rules = [
                'login' => ['required', 'login'],
                'password' => ['required', 'password'],
            ];

            $errors = validateForm($rules, $data);

            if (empty($errors) && userLogin($_POST)) {
                exit('success');
            } else {
                exit('error');
            }
        }
    } else {
        header('Location: /');
    }
}
