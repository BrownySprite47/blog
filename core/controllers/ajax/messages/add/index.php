<?php

/**
 * Page /messages/add
 */
function index()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $data = [
            'name' => clean($_POST['name']),
            'email' => clean($_POST['email']),
            'homepage_url' => clean($_POST['url']),
            'text' => clean($_POST['text']),
        ];

        $rules = [
            'name' => ['required', 'login'],
            'email' => ['required', 'email'],
            'homepage_url' => ['url'],
            'text' => ['required'],
        ];

        $errors = validateForm($rules, $data);

        if (empty($errors) && addMessage($_POST)) {
            exit('success');
        } else {
            exit('error');
        }
    } else {
        header('Location: /');
    }
}
