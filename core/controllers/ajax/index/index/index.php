<?php

/**
 * Page /
 */
function index()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $settings['count_on_page'] = '10';

        $data = getPaginationData($_POST, $settings['count_on_page'], 'messages');

        $limit = get_limit($data['startproject'], $settings['count_on_page']);

        $data['messages'] = getAllSortMessages($_POST, $limit);

        /**
         * Require layouts
         */
        renderView('index/index/layouts/index', $data);
    } else {
        header('Location: /');
    }
}
