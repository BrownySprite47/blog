<?php
/**
 * Page /ajax/index/upload/img
 */
function index()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (!empty($_FILES['file']['name'])) {
            ob_start();

            $filename = stripslashes($_FILES['file']['name']);
            $ext = getExtension($filename);
            $ext = strtolower($ext);

            if ($_FILES['file']['size'] > 5*1048576) {
                echo "<span class='image_name' style='background-image: url(/assets/images/upload_img.png)'></span><p class='errors_image'>Превышен максимально допустимый размер файла (5 МБ).</p>";
            } else {
                $valid_formats = array("jpg", "png", "svg","jpeg");
                if (in_array($ext, $valid_formats)) {
                    $name = time();
                    $newname = 'uploads/'.$name.'.'.$ext;
                    $filename = $name.'.'.$ext;
                    if (move_uploaded_file($_FILES['file']['tmp_name'], $newname)) {
                        echo "<span class='image_name' style='background-image: url(" . CORE_IMG_PATH . $filename . ")'></span> <input style='display: none' value='".$filename."' id=\"image_name\" type=\"text\" name=\"image\" />";
                    } else {
                        echo "<span class='image_name' style='background-image: url(/assets/images/upload_img.png)'></span><p class='errors_image'>Произошла ошибка загрузки изображения! Повторите попытку позднее.</p>";
                    }
                } else {
                    echo "<span class='image_name' style='background-image: url(/assets/images/upload_img.png)'></span><p class='errors_image'>Неверный формат изображения!<br> Поддерживаемые форматы : <br> 'jpg', 'png', 'svg', 'jpeg'</p>";
                }
            }
            $req = ob_get_contents();
            ob_end_clean();
            echo json_encode($req);
        }
    } else {
        header('Location: /');
    }
}
