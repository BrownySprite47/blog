<?php

/**
 * Page /auth/login
 */
function index()
{
    if (!isset($_SESSION['id'])) {
        $data['title'] = 'Sign in';

        $data['js'][] = 'js/auth/login/script.js';

        $data['title'] = 'Sign in';

        /**
         * Require view
         */
        renderView('auth/login/index/index', $data);
    } else {
        header('Location: /');
    }
}
