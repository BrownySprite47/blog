<?php

/**
 * Page /messages/add
 */
function index()
{
    if (isset($_SESSION['id'])) {
        $data['js'][] = 'js/messages/add/script.js';

        $data['title'] = 'Add message';

        /**
         * Require view
         */
        renderView('messages/add/index/index', $data);
    } else {
        header('Location: /');
    }
}
