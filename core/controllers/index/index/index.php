<?php

/**
 * Page /
 */
function index()
{
    $settings['count_on_page'] = '10';

    $data = getPaginationData($_POST, $settings['count_on_page'], 'messages');

    $limit = get_limit($data['startproject'], $settings['count_on_page']);

    $data['messages'] = getAllMessages($limit);

    $data['title'] = 'Home page';

    /**
     * Require view
     */
    renderView('index/index/index/index', $data);

}
