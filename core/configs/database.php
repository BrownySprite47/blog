<?php

/**
 * Database connection settings
 */
return [
   'host' => 'localhost',
   'user' => 'root',
   'pass' => 'root',
   'name' => 'astrio',
];
