<?php

/**
 * Define Root Dir constant
 */
define('CORE_DIR', $_SERVER['DOCUMENT_ROOT']);

/**
 * Define site constant
 */
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
    define('SITE_URL', 'https://'.$_SERVER["SERVER_NAME"]);
} else {
    define('SITE_URL', 'http://'.$_SERVER["SERVER_NAME"]);
}

/**
 * On / Off Debug
 */
define('DEBUG_DB', true);
