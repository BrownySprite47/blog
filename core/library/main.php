<?php

/**
 * Getting segments from url
 *
 * @return bool|string
 */
function getUrnSegments()
{
    $urn = (isset($_GET['urn'])) ? strtolower($_GET['urn']) : '';

    $urn = explode('?', $urn);
    $segments = $urn[0];
    $segment = substr($segments, -1);
    if ($segment == '/') {
        $segments = substr($segments, 0, -1);
    }
    return $segments;
}

/**
 * Displaying page 404
 */
function show404page()
{
    header('HTTP/1.1 404 Not Found');
    renderView('errors/404/index');
}

/**
 * Require views or layouts
 *
 * @param $name
 * @param array $data
 */
function renderView($name, $data = [])
{
    require_once CORE_DIR . '/core/views/' . $name . '.php';
}
