<?php

/**
 * Establishing a connection to the database
 *
 * @return mysqli
 */
function dbConnect()
{
    $db = require CORE_DIR . '/core/configs/database.php';
    $link = @mysqli_connect($db['host'], $db['user'], $db['pass'], $db['name']);
    mysqli_set_charset($link, "utf8");
    if (!$link) {
        if (DEBUG_DB) {
            die(mysqli_error($link));
        } else {
            die(renderView('errors/500/index'));
        }
    }

    return $link;
}

/**
 * Performing a query to the database.
 *
 * @param $sql
 * @param string $id
 * @return bool|int|mysqli_result|string
 */
function dbQuery($sql, $id = '')
{
    $link = dbConnect();
    $result = mysqli_query($link, $sql);

    if (!$result) {
        if (DEBUG_DB) {
            die(mysqli_error($link));
        } else {
            die(renderView('errors/500/index'));
        }
    } elseif ($id != '') {
        return mysqli_insert_id($link);
    }
    return $result;
}

/**
 * Getting data from the database
 *
 * @param $data
 * @return array
 */
function getData($data)
{
    while ($result[] = mysqli_fetch_assoc($data));

    return $result;
}
