<?php

function required($data)
{
    if (empty($data)) {
        return false;
    }
    return true;
}

function login($data)
{
    if (!preg_match('/[a-zA-Z]/', $data)) {
        if (strlen($data) < 5 || strlen($data) > 60) {
            return false;
        }
        return false;
    }
    return true;
}
function password($data)
{
    if (!preg_match('/[a-zA-Z0-9]/', $data)) {
        if (strlen($data) < 5 || strlen($data) > 60) {
            return false;
        }
        return false;
    }
    return true;
}

function email($data)
{
    if (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
        return false;
    }
    return true;
}

function url($data)
{
    if (!filter_var($data, FILTER_VALIDATE_URL)) {
        return false;
    }
    return true;
}

function validateForm($dataWithRules, $data)
{
    $fields = array_keys($dataWithRules);
    $errorForms = [];

    foreach ($fields as $fieldName) {
        $fieldData = $data[$fieldName];
        $rules = $dataWithRules[$fieldName];

        foreach ($rules as $ruleName) {
            if (!$ruleName($fieldData)) {
                $errorForms[$fieldName][] = $ruleName;
            }
        }
    }

    return $errorForms;
}

